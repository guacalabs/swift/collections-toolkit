#import <Foundation/Foundation.h>

//! Project version number for Collections.
FOUNDATION_EXPORT double CollectionsVersionNumber;

//! Project version string for Collections_iOS.
FOUNDATION_EXPORT const unsigned char CollectionsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Collections/PublicHeader.h>
