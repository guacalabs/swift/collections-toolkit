import Foundation

// MARK: - Mapping

public extension Sequence {
    /// Returns an array containing the results of mapping by the given KeyPath.
    /// - Parameter keyPath: KeyPath to map by.
    func map<T>(_ keyPath: KeyPath<Element, T>) -> [T] {
        return map { $0[keyPath: keyPath] }
    }
}

// MARK: - Sorting

public extension Sequence {
    /// Returns the elements of the sequence, sorted by the given key-path in ascending order.
    /// - Parameter keyPath: KeyPath to sort by.
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return sorted { a, b in
            return a[keyPath: keyPath] < b[keyPath: keyPath]
        }
    }

    /// Returns the elements of the sequence, sorted by the given key-path and using the given predicate as the comparison between elements.
    /// - Parameter keyPath: KeyPath to sort by.
    /// - Parameter areInIncreasingOrder: A predicate that returns true if its first argument should be ordered before its second argument; otherwise, false.
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>, using areInIncreasingOrder: (T, T) -> Bool) -> [Element] {
        return sorted { a, b in
            return areInIncreasingOrder(a[keyPath: keyPath], b[keyPath: keyPath])
        }
    }
}

// MARK: - Grouping

public extension Sequence {
    /// Creates a new dictionary whose keys are the groupings returned by the given key-path and whose values are arrays of the elements that returned each key.
    /// - Parameter keyPath: KeyPath to group by.
    func grouped<T: Hashable>(by keyPath: KeyPath<Element, T>) -> [T: [Element]] {
        return .init(grouping: self, by: { $0[keyPath: keyPath] })
    }
}

// MARK: - Unique Values

public extension Sequence where Iterator.Element: Equatable {
    /// Returns a new sequence by removing duplicated elements.
    func removingDuplicates() -> [Iterator.Element] {
        return reduce([]) { collection, element in
            collection.contains(element)
                ? collection
                : collection + [element]
        }
    }
}

public extension Sequence where Iterator.Element: Equatable & Hashable {
    /// Returns a new sequence by removing duplicated elements.
    func removingDuplicates() -> [Iterator.Element] {
        var dictionary: [Element: Bool] = [:]
        return filter { dictionary.updateValue(true, forKey: $0) == .none }
    }
}
