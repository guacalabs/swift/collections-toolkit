// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "CollectionsToolkit",
    platforms: [
        .iOS(.v11),
        .tvOS(.v11),
        .macOS(.v10_14)
    ],
    products: [
        .library(
            name: "CollectionsToolkit",
            targets: ["CollectionsToolkit"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "CollectionsToolkit",
            dependencies: [],
            path: "./Sources"
        ),
        .testTarget(
            name: "CollectionsToolkitTests",
            dependencies: ["CollectionsToolkit"],
            path: "./Tests"
        )
    ],
    swiftLanguageVersions: [.v5]
)
