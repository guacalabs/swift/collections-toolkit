import Foundation

enum Test {}

// MARK: - Test.HashableElement

extension Test {
    struct HashableElement: Hashable {
        let id: String
    }
}

// MARK: - Test.EquatableObject

extension Test {
    struct EquatableElement: Equatable {
        let id: String
    }
}
