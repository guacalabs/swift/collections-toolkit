import XCTest
import CollectionsToolkit

final class Sequence_Tests: XCTestCase {
    func test_map() {
        // given
        let elements: [Test.HashableElement] = [.init(id: "A"), .init(id: "B")]
        // when
        let elementIDs = elements.map(\.id)
        // then
        XCTAssertEqual(["A", "B"], elementIDs)
    }

    func test_sorted() {
        // given
        let elements: [Test.HashableElement] = [.init(id: "D"), .init(id: "B"), .init(id: "C"), .init(id: "A")]
        // when
        let elementIDs = elements
            .sorted(by: \.id)
            .map(\.id)
        // then
        XCTAssertEqual(["A", "B", "C", "D"], elementIDs)
    }

    func test_sorted_ascending() {
        // given
        let elements: [Test.HashableElement] = [.init(id: "D"), .init(id: "B"), .init(id: "C"), .init(id: "A")]
        // when
        let elementIDs = elements
            .sorted(by: \.id, using: <)
            .map(\.id)
        // then
        XCTAssertEqual(["A", "B", "C", "D"], elementIDs)
    }

    func test_sorted_descending() {
        // given
        let elements: [Test.HashableElement] = [.init(id: "D"), .init(id: "B"), .init(id: "C"), .init(id: "A")]
        // when
        let elementIDs = elements
            .sorted(by: \.id, using: >)
            .map(\.id)
        // then
        XCTAssertEqual(["D", "C", "B", "A"], elementIDs)
    }

    func test_grouped() {
        // given
        let elements: [Test.HashableElement] = [.init(id: "A"), .init(id: "B"), .init(id: "C"), .init(id: "A")]
        // when
        let groups = elements.grouped(by: \.id)
        // then
        XCTAssertEqual(2, groups["A"]?.count)
        XCTAssertEqual(1, groups["B"]?.count)
        XCTAssertEqual(1, groups["C"]?.count)
    }

    func test_removingDuplicates_whenEquatableAndHashable() {
        // given
        let elements: [Int] = [1, 1, 2, 2, 3, 3]
        // when
        let uniqueElements = elements.removingDuplicates()
        // then
        XCTAssertEqual([1, 2, 3], uniqueElements)
    }

    func test_removingDuplicates_whenEquatableOnly() {
        // given
        let elements: [Test.EquatableElement] = [
            .init(id: "1"), .init(id: "1"),
            .init(id: "2"), .init(id: "2"),
            .init(id: "3"), .init(id: "3")
        ]
        // when
        let uniqueElements: [Test.EquatableElement] = elements.removingDuplicates()
        // then
        let expectedElements: [Test.EquatableElement] = [.init(id: "1"), .init(id: "2"), .init(id: "3")]
        XCTAssertEqual(expectedElements, uniqueElements)
    }
}
